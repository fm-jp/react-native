import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  ToolbarAndroid,
  Alert,
  Image
} from 'react-native';
import { Card, Button } from 'react-native-material-design';

class MySceneComponent extends Component {
  render() {
    return (
      <View style={{backgroundColor: '#EFEFEF', flex: 1}}>
        <ToolbarAndroid
          style={{height: 56, backgroundColor: '#4ed4d8'}}
          title="App"
          navIcon={require('image!ic_menu_white_24dp')}
          titleColor="#FFF"
          actions={toolbarActions}
          onActionSelected={this._onActionSelected}
          />
        <View style={{backgroundColor: '#DDD', flex:1, paddingTop: 5}}>
          <Card>
            <Card.Media
              image={<Image source={{uri: 'http://www.favoritemedium.com/uploads/2015/12/background_goldengate_v2__1455771440_112.208.196.54.jpg'}} />}
              overlay
              >
              <Text style={{fontSize: 18, color: '#FFF', fontWeight: 'bold'}}>San Francisco</Text>
              <Text style={{color: '#FFF'}}>California, US</Text>
            </Card.Media>
            <Card.Body>
              <Text>San Francisco, California</Text>
            </Card.Body>
            <Card.Actions position="right">
              <Button text="ACTION" />
            </Card.Actions>
          </Card>
        </View>
      </View>
    );
  }

  _onActionSelected(position) {
    Alert.alert('App', toolbarActions[position].title);
  }
};

class Idea extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{name: 'My first Scene', index: 0}}
        renderScene={(route, navigator) =>
          <MySceneComponent
            name={route.name}
            onForward={() => {
              var nextIndex = route.index + 1;
              navigator.push({
                name: 'Scene ' + nextIndex,
                index: nextIndex
              });
            }}
            onBack={() => {
              if (route.index > 0) {
                navigator.pop();
              }
            }}
            />
        }
      />
    );
  }
}

const toolbarActions = [
  { title: 'Invites', show: 'always'}
];

AppRegistry.registerComponent('Idea', () => Idea);
